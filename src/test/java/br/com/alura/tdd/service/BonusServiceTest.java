package br.com.alura.tdd.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import br.com.alura.tdd.modelo.Funcionario;
import java.math.BigDecimal;
import java.time.LocalDate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class BonusServiceTest {

  BonusService bonusService;
  Funcionario funcionario;

  @BeforeEach
  void setUp() {
    this.bonusService = new BonusService();
    this.funcionario = new Funcionario("Rodrigo", LocalDate.now(), BigDecimal.ZERO);
  }

  @Test
  void bonusDeveriaSer0ParaFuncionarioComSalarioMuitoAlto() {
    this.funcionario.setSalario(BigDecimal.valueOf(25000));
    assertThrows(IllegalArgumentException.class,
        () -> bonusService.calcularBonus(this.funcionario));
    try {
      bonusService.calcularBonus(this.funcionario);
      fail();
    } catch (Exception e) {
      assertEquals("Funcionário com salário maior que R$10000 não pode receber bônus.",
          e.getMessage());
    }
  }

  @Test
  void bonusDeveriaSer10PorCentoDoSalario() {
    this.funcionario.setSalario(BigDecimal.valueOf(2500));
    BigDecimal bonus = bonusService.calcularBonus(this.funcionario);
    assertEquals(new BigDecimal("250.00"), bonus);
  }

  @Test
  void bonusDeveriaSer10PorCentoParaSalarioDeExatamente10k() {
    this.funcionario.setSalario(BigDecimal.valueOf(10000));
    BigDecimal bonus = bonusService.calcularBonus(this.funcionario);
    assertEquals(new BigDecimal("1000.00"), bonus);
  }
}