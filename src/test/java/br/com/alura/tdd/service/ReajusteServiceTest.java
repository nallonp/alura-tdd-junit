package br.com.alura.tdd.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import br.com.alura.tdd.modelo.Desempenho;
import br.com.alura.tdd.modelo.Funcionario;
import java.math.BigDecimal;
import java.time.LocalDate;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ReajusteServiceTest {

  ReajusteService reajusteService;
  Funcionario funcionario;

  @BeforeEach
  void setUp() {
    System.out.println("Antes de cada.");
    this.reajusteService = new ReajusteService();
    this.funcionario = new Funcionario("Ana", LocalDate.now(), new BigDecimal("1000"));
  }

  @AfterEach
  void tearDown() {
    System.out.println("Depois de cada.");
  }

  @BeforeAll
  static void startUp() {
    System.out.println("Antes de todos.");
  }

  @AfterAll
  static void shutdown() {
    System.out.println("Depois de todos.");
  }

  @Test
  void reajusteDeveriaSerDe3PorCentoQuandoDesempenhoForADesejar() {
    reajusteService.concederReajuste(funcionario, Desempenho.A_DESEJAR);
    assertEquals(new BigDecimal("1030.00"), funcionario.getSalario());
  }

  @Test
  void reajusteDeveriaSerDe15PorCentoQuandoDesempenhoForBom() {
    reajusteService.concederReajuste(funcionario, Desempenho.BOM);
    assertEquals(new BigDecimal("1150.00"), funcionario.getSalario());
  }

  @Test
  void reajusteDeveriaSerDe20PorCentoQuandoDesempenhoForOtimo() {
    reajusteService.concederReajuste(funcionario, Desempenho.OTIMO);
    assertEquals(new BigDecimal("1200.00"), funcionario.getSalario());
  }
}
